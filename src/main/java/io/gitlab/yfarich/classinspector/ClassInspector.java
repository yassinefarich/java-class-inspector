package io.gitlab.yfarich.classinspector;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public class ClassInspector {

  private Class classToInspect;
  private List<String> packagesList = Collections.emptyList();
  private List<String> inspectionResult = new ArrayList<>();
  private Set<String> inspectedTypes = new HashSet<>();

  public ClassInspector(Class clazz) {
    classToInspect = clazz;
  }

  public List<String> inspectPackages(String... packages) {
    packagesList = new ArrayList<>(Arrays.asList(packages));
    inspect(classToInspect);
    return inspectionResult;
  }

  private void inspect(Class clazz)
      throws IllegalArgumentException {

    if (inspectedTypes.contains(clazz.getCanonicalName())) {
      return;
    }
    inspectedTypes.add(clazz.getCanonicalName());

    Field[] fields = clazz.getDeclaredFields();

    for (Field field : fields) {
      String fieldName = field.getName();
      Class<?> fieldClass = field.getType();

      if (fieldClass.isPrimitive()) {
        inspectionResult
            .add(String.format("Primitive field (%s) of : %s", fieldName, fieldClass.getName()));
        System.out.println("Primitive field found : " + fieldName);
        continue;
      }

      Type genericType = field.getGenericType();
      if (genericType instanceof ParameterizedType) {

        ParameterizedType integerListType = (ParameterizedType) field.getGenericType();
        Arrays.stream(integerListType.getActualTypeArguments())
            .filter(filterInPackages())
            .filter(type -> type instanceof Class)
            .forEach(type -> inspect((Class) type)
            );
      }

      if (filterInPackages().test(fieldClass)) {
        inspectionResult.add(
            String.format("Field (%s) found of type : %s", fieldName, fieldClass.getName()));

        System.out.printf("--> Field (%s) found of type : %s\n", fieldName, fieldClass.getName());
        inspect(field.getType());
      } else {
        inspectionResult.add(String
            .format("Skipping field %s class : %s", fieldName, fieldClass.getCanonicalName()));
        System.out.printf("Skipping class scan: %s\n", fieldClass.getCanonicalName());
      }
    }
  }

  private Predicate<? super Type> filterInPackages() {
    return type -> packagesList.stream().anyMatch(pack -> type.getTypeName().startsWith(pack));
  }

}