package io.gitlab.yfarich.classinspector.beans;

public class Country {

	public String name;
	private Double population;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPopulation() {
		return population;
	}
	public void setPopulation(Double population) {
		this.population = population;
	}
	
	
}
