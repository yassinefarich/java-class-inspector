package io.gitlab.yfarich.classinspector;

import io.gitlab.yfarich.classinspector.beans.City;
import org.junit.Test;

public class ClassInspectorTest {

  @Test
  public void inspectPackages() {
    new ClassInspector(City.class).inspectPackages("io.gitlab", "java.lang");
  }
}